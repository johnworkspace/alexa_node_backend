/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk-core');
const request = require('request');


var prices;
prices = [{price: 'twenty five', store:'REWE', brand:'Ja'},
          {price: 'two', store:'PENNY', brand:'Ja'},
          {price: 'thirty one', store:'ALDI', brand:'Penny'},
          {price: 'two', store:'ABC STORE', brand:'ALDI'},
          {price: 'fifteen', store:'REWE', brand:'Ja'},
          {price: 'eighteen', store:'PENNY', brand: 'Lidl'},
          {price: 'thirty one', store:'ALDI', brand:'Bell'},
          {price: 'fourty three', store:'ABC STORE', brand:'Dennre'},
          {price: 'seven', store:'PENNY', brand: 'Veganz'},
          {price: 'one', store:'ALDI', brand:'Alnatura'},
          {price: 'eleven', store:'ABC STORE', brand:'Dennre'}
          ]; 

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const speechText = 'Welcome to Shop Con, your best shopping companion, what would you like to buy today son?';


    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Welcome to Shop Con, your best shopping companion, what do you like to buy today?, son', speechText)
      .getResponse();
  },
};

const FindCheapestItemHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'findCheapestItem';
  },
  handle(handlerInput) {
    
    var accessToken = handlerInput.requestEnvelope.context.System.apiAccessToken;
    
    var options = {
    url: 'https://api.amazonalexa.com/v2/householdlists/',
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization' : accessToken
    }
};
    
    
    request(options, function(err, res, body) {
      
      
      if(!err){
        let json = JSON.parse(body);
        console.log(json);
      }
    });
    
    var randomIndex;
    randomIndex = Math.floor((Math.random() * (prices.length-1))+1);
    console.log('random value: ' + randomIndex);
    var item = handlerInput.requestEnvelope.request.intent.slots.item.value;
    var location = handlerInput.requestEnvelope.request.intent.slots.location.value;
    console.log(item);
    var locationText = (location!=undefined)?' near '+location:'';
    
    //const speechText = 'You can say hello to me!';
    
    
   var speechText = 'The best offer is ' + 
   prices[randomIndex].brand + ' ' +
   item +    
   ' in ' + 
  prices[randomIndex].store +
   //locationText +
  ' for ' + 
  prices[randomIndex].price + ' euros , can I help you with something else?';
  
    
    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt("would you like to buy something else?")
      .getResponse();
  },
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'You can say hello to me!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Goodbye!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};


const FallbackIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent');
  },
  handle(handlerInput) {
    const speechText = 'I did\'nt catch that one, could you please say it again';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};


const ExitSessionHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'exitSession';
  },
  handle(handlerInput) {
    
  
    const speechText = 'Alright, dont forget your shopping bags, have a nice day and happy shopping, honey';

    
    return handlerInput.responseBuilder
      .speak(speechText)
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    FindCheapestItemHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler,
    FallbackIntentHandler,
    ExitSessionHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();


